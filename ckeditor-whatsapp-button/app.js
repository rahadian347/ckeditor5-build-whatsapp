import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import UpcastWriter from '@ckeditor/ckeditor5-engine/src/view/upcastwriter';
import DowncastWriter from '@ckeditor/ckeditor5-engine/src/view/downcastwriter';
import imageIcon from './icon.svg';

/**
 * @module module:engine/view/upcastwriter
 */

export default class WhatsappButton extends Plugin {
	init() {
		const editor = this.editor;

		editor.ui.componentFactory.add( 'WhatsappButton', locale => {
			const view = new ButtonView( locale );

			view.set( {
				label: 'Whatsapp Link',
				icon: imageIcon,
				tooltip: true,
			} );

			// Callback executed once the image is clicked.
			view.on( 'execute', () => {
				const number = prompt( 'Whatsapp number' );
				const url = 'https://wa.me/'+number;

				editor.model.change( downCastWriter => {
					// downCastWriter = new DowncastWriter();
					const content = '<p><a href="' + url + '">Whatsapp 😉</a></p>';
					const viewFragment = editor.data.processor.toView(content);
					const modelFragment = editor.data.toModel(viewFragment);

					// 	Insert the image in the current selection location.
					editor.model.insertContent( modelFragment, editor.model.document.selection );
				} );

				// editor.model.change(downCastWriter => {
				// 	const buttonElement1 = downCastWriter.createAttributeElement('a', { href: 'foo.bar' }, { priority: 5 });

				// 	// Insert the image in the current selection location.
				// 	editor.model.insertContent(buttonElement1, editor.model.document.selection);
				// });
			} );

			return view;
		} );
	}
}
